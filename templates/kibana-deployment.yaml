{{- /*
    Copyright 2021 floragunn GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/}}

{{ if (not .Values.common.external_ca_certificates_enabled) }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "searchguard.fullname" . }}-kibana
  namespace: {{ .Release.Namespace }}
  labels:
    app: {{ template "searchguard.fullname" . }}
    chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
    release: "{{ .Release.Name }}"
    heritage: "{{ .Release.Service }}"
    component: {{ template "searchguard.fullname" . }}-kibana
    role: kibana
spec:
  replicas: {{ .Values.kibana.replicas }}
  strategy:
    type: RollingUpdate
  selector:
    matchLabels:
      component: {{ template "searchguard.fullname" . }}
      role: kibana
  template:
    metadata:
      labels:
        release: "{{ .Release.Name }}"
        app: {{ template "searchguard.fullname" . }}
        component: {{ template "searchguard.fullname" . }}
        role: kibana
        {{- if .Values.kibana.labels }}
{{ toYaml .Values.kibana.labels | indent 8 }}
        {{- end }}
      annotations:
        {{ if .Values.common.restart_pods_on_config_change }}
        checksum/config: {{ include (print $.Template.BasePath "/kibana-configmap.yaml") . | sha256sum }}
        {{ end }}
      {{- if .Values.kibana.annotations }}
{{ toYaml .Values.kibana.annotations | indent 8 }}
      {{- end }}
    spec:
      subdomain: {{ template "searchguard.fullname" . }}
      serviceAccountName: {{ template "searchguard.fullname" . }}
      securityContext:
        fsGroup: 1000
      {{- if eq .Values.kibana.antiAffinity "hard" }}
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - topologyKey: "topology.kubernetes.io/zone"
              labelSelector:
                matchLabels:
                  component: {{ template "searchguard.fullname" . }}
                  role: kibana
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            podAffinityTerm:
              topologyKey: "kubernetes.io/hostname"
              labelSelector:
                matchLabels:
                  component: {{ template "searchguard.fullname" . }}
                  role: kibana
      {{- else if eq .Values.kibana.antiAffinity "soft" }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            podAffinityTerm:
              topologyKey: "topology.kubernetes.io/zone"
              labelSelector:
                matchLabels:
                  component: {{ template "searchguard.fullname" . }}
                  role: kibana
          - weight: 2
            podAffinityTerm:
              topologyKey: "kubernetes.io/hostname"
              labelSelector:
                matchLabels:
                  component: {{ template "searchguard.fullname" . }}
                  role: kibana
      {{- end }}
      initContainers:
        - name: kibana-init
          {{- if or (semverCompare "<7.11" .Values.common.elkversion)  (semverCompare ">7.14.0" .Values.common.elkversion) }}
          image: "{{ .Values.common.images.repository }}/{{ .Values.common.images.provider }}/{{ .Values.common.images.sgadmin_base_image }}:{{ .Values.common.elkversion }}-{{ .Values.common.sgversion }}"
          {{- else }}
          image: "{{ .Values.common.images.repository }}/{{ .Values.common.images.provider }}/{{ .Values.common.images.sgadmin_base_image }}:7.10.2-49.0.0"
          {{- end }}
          imagePullPolicy: {{ .Values.common.pullPolicy }}
          command:
            - sh
            - -c
            - |
                #!/usr/bin/env bash -e
                until kubectl get secrets {{ template "searchguard.fullname" . }}-passwd-secret  -o jsonpath="{.data.KIBANA_COOKIE_PWD}"; do
                  echo 'Wait for {{ template "searchguard.fullname" . }}-passwd-secret';
                  sleep 10 ;
                done

                echo "OK, {{ template "searchguard.fullname" . }}-passwd-secret exists now"
          resources:
            limits:
              cpu: "500m"
              memory: 256Mi
            requests:
              cpu: 100m
              memory: 256Mi
        - name: kibana-cookie-config
          {{- if or (semverCompare "<7.11" .Values.common.elkversion)  (semverCompare ">7.14.0" .Values.common.elkversion) }}
          image: "{{ .Values.common.images.repository }}/{{ .Values.common.images.provider }}/{{ .Values.common.images.sgadmin_base_image }}:{{ .Values.common.elkversion }}-{{ .Values.common.sgversion }}"
          {{- else }}
          image: "{{ .Values.common.images.repository }}/{{ .Values.common.images.provider }}/{{ .Values.common.images.sgadmin_base_image }}:7.10.2-49.0.0"
          {{- end }}
          imagePullPolicy: {{ .Values.common.pullPolicy }}
          command:
            - sh
            - -c
            - |
              #!/usr/bin/env bash -e
              cp /tmp/kibana.yml /tmp/config/
              echo $KIBANA_COOKIE_PWD
              #Temporary workaround to pass KIBANA_COOKIE_PWD to kibana.yml
              sed -i -e "s#\${KIBANA_COOKIE_PWD}#$KIBANA_COOKIE_PWD#g" /tmp/config/kibana.yml
              cat /tmp/config/kibana.yml
              chown -R 1000:1000 /tmp/config
          resources:
            limits:
              cpu: "500m"
              memory: 256Mi
            requests:
              cpu: 100m
              memory: 256Mi
          envFrom:
            - secretRef:
                name: {{ template "searchguard.fullname" . }}-passwd-secret
          volumeMounts:
              - mountPath: /tmp/kibana.yml
                name: config
                subPath: kibana.yml
              - mountPath: /tmp/config
                name: kibanaconfig
{{ include "searchguard.generate-certificates-init-container" . | indent 8 }}
      {{ if .Values.common.es_upgrade_order }}
{{ include "searchguard.kibana-wait-container" . | indent 8 }}
      {{ end }}
      {{ if .Values.common.docker_registry.enabled }}
      imagePullSecrets:
      - name: {{ .Values.common.docker_registry.imagePullSecret }}
      {{ end }}
      containers:
      - name: kibana
        securityContext:
          capabilities:
            add:
              - IPC_LOCK
              - SYS_RESOURCE
      {{ if .Values.common.xpack_basic }}
        image: "{{ .Values.common.images.repository }}/{{ .Values.common.images.provider }}/{{ .Values.common.images.kibana_base_image }}:{{ .Values.common.elkversion }}-{{ .Values.common.sgkibanaversion }}"
      {{ else }}
        image: "{{ .Values.common.images.repository }}/{{ .Values.common.images.provider }}/{{ .Values.common.images.kibana_base_image }}:{{ .Values.common.elkversion }}-oss-{{ .Values.common.sgkibanaversion }}"
      {{ end }}
        imagePullPolicy: {{ .Values.common.pullPolicy }}
        lifecycle:
          postStart:
{{ include "searchguard.remove-demo-certs" . | indent 12 }}
          preStop:
{{ include "searchguard.lifecycle-cleanup-certs" . | indent 12 }}
        envFrom:
        - secretRef:
            name: {{ template "searchguard.fullname" . }}-passwd-secret
        env:
        - name: NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: DISCOVERY_SERVICE
          value: {{ template "searchguard.fullname" . }}-clients.{{ .Release.Namespace }}.svc
        {{- range $key, $value :=  .Values.common.env }}
        - name: {{ $key | upper | replace "-" "_" }}
          value: {{ $value | quote }}
        {{- end }}
        {{- range $key, $value :=  .Values.kibana.env }}
        - name: {{ $key | upper | replace "-" "_" }}
          value: {{ $value | quote }}
        {{- end }}
        ports:
        - containerPort: 5601
          name: http
          protocol: TCP
        livenessProbe:
          exec:
            command:
              - pgrep
              - node
          initialDelaySeconds: 60
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: api/status
            port: http
            scheme: HTTPS
          initialDelaySeconds: 60
          timeoutSeconds: 5
        resources:
{{ toYaml .Values.kibana.resources | indent 10 }}
        volumeMounts:
          - mountPath: /usr/share/kibana/config/
            name: kibanaconfig
          - name: certificates-secrets-volume
            readOnly: true
            mountPath: "/usr/share/kibana/config/certificates-secrets"
      volumes:
        - name: certificates-secrets-volume
          secret:
            secretName: {{ template "searchguard.fullname" . }}-nodes-cert-secret
            defaultMode: 0644
        - configMap:
            name: {{ template "searchguard.fullname" . }}-kibana-config
            defaultMode: 0666
          name: config
        - name: kubectl
          emptyDir: {}
        - name: kibanaconfig
          emptyDir: {}
{{ end }}